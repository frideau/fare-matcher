;;; -*- Lisp -*-

(asdf:defsystem :fare-quasiquote-matcher
  :depends-on (:fare-quasiquote :fare-matcher)
  :components ((:file "fare-quasiquote-matcher")))
